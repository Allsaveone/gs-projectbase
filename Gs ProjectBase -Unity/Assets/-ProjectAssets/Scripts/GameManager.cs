﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using System.Linq;

public class GameManager : MonoBehaviour {

    #region Singleton
    private static GameManager instance = null;

    public static GameManager Instance()
    {
        if (instance != null)
        {
            return instance;
        }
        Debug.Log("GM| Not Initialized...");
        return instance;
    }
    #endregion
    [Header("UserProfile:")]
    public Utility_UserProfile userProfile;

    [Header("InGame Ui Elements:")]
    public Button quiz_Button;
    public Button ranking_Button;//Lbs
    public Button friends_Button;
    public Button options_Button;

    [Header("User Profile Ui Elements:")]
    public Utility_UserProfile profile;

    [Header("Game Ui Elements:")]
    public Button true_Button;
    public Button false_Button;
    public Button reTry_Button;
    public Button finished_Button;
    public Button Continue_Button;

    public Text question_Text;
    public Image question_Image;
    public Image result_Image;

    public Image ProgessBar_Image;

    [Header("Quiz Sprites:")]
    public Sprite positiveResult_Sprite;
    public Sprite negativeResult_Sprite;
    public Sprite complete_Sprite;

    [Header("Game Questions:")]
    public List<Question> questions;
    public List<Question> unAnsweredQuestions = new List<Question>();
    private Question currentQuestion;

    [Header("Logo Animation:")]
    public Animator logoAnimator;

    [Header("Shaker:")]
    public Utility_Shake shaker;

    //Privates/Hidden variables
    private int questionCount;
    [HideInInspector]
    public int correctAnswers = 0;
    //[HideInInspector]
    public int answered = 0;
    [Header("TESTING!:")]
    public float questionDelay = 0.25f;
    public GameSparksManager.PlayerDetails currentPlayerDetails;

    void Awake()
    {
        if (instance == null)
        {
            // when the first GM is activated, it should be null, so we create the reference
            Debug.Log("GM| Singleton Initialized...");
            instance = this;
        }
        else
        {
            // if we load into a level that has the game manager present, it should be destroyed //
            Debug.Log("GM| Removed Duplicate...");
            Destroy(this.gameObject);
        }
    }

    public void StorePlayerProfile(GameSparksManager.PlayerDetails playerDetails)
    {
        currentPlayerDetails = playerDetails;
        SetPlayerProfile(currentPlayerDetails);
    }

    public void SetPlayerProfile(GameSparksManager.PlayerDetails playerDetails)
    {
        userProfile.SetUserProfileUI(playerDetails.displayName, (int)playerDetails.level, (int)playerDetails.score);
    }

    public void InitGame ()
    {
        reTry_Button.gameObject.SetActive(false);
        finished_Button.gameObject.SetActive(false);
        Continue_Button.gameObject.SetActive(false);

        true_Button.gameObject.SetActive(true);
        false_Button.gameObject.SetActive(true);

        if (unAnsweredQuestions == null || unAnsweredQuestions.Count != questions.Count)
        {
            unAnsweredQuestions = questions;
            questionCount = unAnsweredQuestions.Count;
        }

        AdjustProgessBar(0.01f);
        SetRandomQuestion();

        Debug.LogWarning("InitGame");
    }

    void SetRandomQuestion()
    {
        if (unAnsweredQuestions.Count == 0)
        {
            QuizFinished();
            return;
        }

        int randomNumber = Random.Range(0, unAnsweredQuestions.Count);
        currentQuestion = unAnsweredQuestions[randomNumber];

        result_Image.gameObject.SetActive(false);
        result_Image.sprite = null;
        result_Image.color = Color.white;

        question_Image.color = Color.white;
        question_Text.text = currentQuestion.question;

        true_Button.interactable = true;
        false_Button.interactable = true;
    }

    public void SelectAnswer(string answer)
    {
        true_Button.interactable = false;
        false_Button.interactable = false;

        StopAllCoroutines();
        AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);
        answered++;

        StartCoroutine(ProcessAnswer(answer));
    }

    IEnumerator ProcessAnswer(string answer)
    {
        yield return new WaitForSeconds(questionDelay);

        if (currentQuestion.isTrue && answer == "True")
        {
            RightAnswer();
        }
        else if (!currentQuestion.isTrue && answer == "False")
        {
            RightAnswer();
        }
        else
        {
            WrongAnswer();
        }
    }

    void RightAnswer()
    {
        result_Image.sprite = positiveResult_Sprite;
        result_Image.color = Color.green;
        result_Image.gameObject.SetActive(true);

        correctAnswers++;
        question_Image.color = Color.green;

        StartCoroutine(ToNextQuestion());
        AudioManager.Instance().PlaySfx(AudioManager.Instance().positive_Sfx);
    }

    void WrongAnswer()
    {
        result_Image.sprite = negativeResult_Sprite;
        result_Image.color = Color.red;
        result_Image.gameObject.SetActive(true);

        shaker.Shake(questionDelay/2, 5f);

        question_Image.color = Color.red;

        StartCoroutine(ToNextQuestion());
        AudioManager.Instance().PlaySfx(AudioManager.Instance().negitive_Sfx);
    }

    IEnumerator ToNextQuestion()
    {
        true_Button.interactable = false;
        false_Button.interactable = false;

        unAnsweredQuestions= SafeRemoveOperation(unAnsweredQuestions, unAnsweredQuestions.IndexOf(currentQuestion));
        AdjustProgessBar(answered);

        yield return new WaitForSeconds(questionDelay);
        SetRandomQuestion();
    }

    List<Question> SafeRemoveOperation(List<Question> target,int index)
    {
        List<Question> tmp = new List<Question>();
        tmp.AddRange(target);
        tmp.RemoveAt(index);
        return tmp;
    }

    public void QuizFinished()
    {
        true_Button.gameObject.SetActive(false);
        false_Button.gameObject.SetActive(false);

        GameSparksManager.Instance().SetBestScore("PostQuizScore", correctAnswers, OnScoreSuccess,OnScoreError);

        result_Image.sprite = complete_Sprite;
        result_Image.color = Color.yellow;
        result_Image.gameObject.SetActive(true);

        AudioManager.Instance().PlaySfx(AudioManager.Instance().completed_Sfx);

        question_Image.color = Color.white;
        string tally = "\n"+"Quiz Complete! \n You got" + correctAnswers + "/" + questionCount + " Right!!";
        question_Text.text = tally;

        correctAnswers = 0;
        answered = 0;
        unAnsweredQuestions.Clear();      
    }

    public void OnScoreSuccess(GameSparks.Core.GSData response)
    {

        if (response.GetNumber("award") != null) // balance might not be present if the player has just registered. so in this case, we  set the balance to zero
        {
            int award = (int)response.GetNumber("award");
            Debug.Log("Gm| OnScoreSuccess Awarded:" + award);
            string tally = question_Text.text + "\n "+"Awarded: " +award+"Points!"+" \n"+" Well Done!";
            question_Text.text = tally;

            //userProfile.AddScore(award);
            currentPlayerDetails.score += award;
            SetPlayerProfile(currentPlayerDetails);
        }

        finished_Button.gameObject.SetActive(true);
        Continue_Button.gameObject.SetActive(true);
        reTry_Button.gameObject.SetActive(true);
    }

    void OnScoreError(string error)
    {
        Debug.LogWarning("GM| OnScoreError:"+ error);
        string tally = question_Text.text + " \n Error posting score: \n " + error;
        question_Text.text = tally;

        finished_Button.gameObject.SetActive(true);
        //Continue_Button.gameObject.SetActive(true);
        reTry_Button.gameObject.SetActive(true);
    }

    void ResetGame()
    {
        finished_Button.gameObject.SetActive(false);
        reTry_Button.gameObject.SetActive(false);
        Continue_Button.gameObject.SetActive(false);
        InitGame();
    }

    void AdjustProgessBar(float amount)
    {
        ProgessBar_Image.fillAmount = amount / questionCount;
    }

    public void OnClick_FinishButton()
    {
        quiz_Button.gameObject.SetActive(true);
        GameSparksUI.Instance().SwitchUi(GameSparksUI.UiState.InGameMenu);
        AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);
    }

    public void OnClick_ReTryButton()
    {
        AudioManager.Instance().PlaySfx(AudioManager.Instance().positive_Sfx);
        ResetGame();
    }

    public void OnClick_ContinueButton()
    {
        AudioManager.Instance().PlaySfx(AudioManager.Instance().positive_Sfx);

        GameSparksUI.Instance().SwitchUi(GameSparksUI.UiState.VideoContent);
        //UnlockableContent
    }

    #region InGame Menu Buttons
    public void QuizButton()
    {
        AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);
        Debug.LogWarning("GM| Start Game Button");
        return;

        quiz_Button.gameObject.SetActive(false);
        GameSparksUI.Instance().SwitchUi(GameSparksUI.UiState.InGame);
    }

    public void RankButton()
    {
        AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);
    }

    public void FriendsButton()
    {
        AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);
    }

    public void OptionsButton()
    {
        AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);
    }
    #endregion
}

[System.Serializable]
public class Question
{
    public string question;
    public bool isTrue;
}
