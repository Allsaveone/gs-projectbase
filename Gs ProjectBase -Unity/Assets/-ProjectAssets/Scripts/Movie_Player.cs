﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Movie_Player : MonoBehaviour {

    public VideoPlayer videoPlayer;
    public VideoClip videoClip;
    public AudioSource audioSource;

    void Start ()
    {
        videoPlayer.clip = videoClip;
        videoPlayer.SetTargetAudioSource(0, audioSource);

        videoPlayer.loopPointReached += VideoPlayer_loopPointReached;
    }

    private void VideoPlayer_loopPointReached(VideoPlayer source)
    {
        videoPlayer.Stop();
        GameSparksUI.Instance().SwitchUi(GameSparksUI.UiState.InGameMenu);

        Debug.LogError("Level Up Event");
        //GameSparksManager.Instance().LevelUpUser();
    }

    public void OnClick_VideoPlay()
    {
        AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);
        videoPlayer.Play();
    }

    public void OnClick_VideoPause()
    {
        AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);
        videoPlayer.Pause();
    }

    public void OnClick_VideoStop()
    {
        AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);
        videoPlayer.Stop();
        GameSparksUI.Instance().SwitchUi(GameSparksUI.UiState.InGameMenu);

        Debug.LogError("Level Up Event");
        //GameSparksManager.Instance().LevelUpUser();
    }
}
