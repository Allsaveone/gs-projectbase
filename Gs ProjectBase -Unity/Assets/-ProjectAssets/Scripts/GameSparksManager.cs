﻿using GameSparks.Core;
using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using GameSparks.Api.Responses;
using GameSparks.Api.Requests;
//using Facebook.Unity;
//using System;
/// <summary>
/// Game Sparks Manager.
/// This class is an RPC which includes all calls to the GameSparks servers and defines responses based on what the server returns.
/// </summary>
/// 
public class GameSparksManager : MonoBehaviour
{
    #region Singleton

    /// <summary>
    ///  A reference to the game-state manager object used for the singleton
    /// </summary>
    private static GameSparksManager instance = null;

    /// <summary>
    /// This method returns the current instance of this class.
    /// The singleton ensures that only one instance of the game-state manager exists.
    /// </summary>
    /// <returns>The instance of GameSparksManager, or null if none is set yet</returns>
    public static GameSparksManager Instance()
    {
        if (instance != null)
        {
            if (!GS.Available && GS.Instance == null)
            {
                Debug.LogError("GSM| Connection Lost, attempting to Reconnect...");
                GS.Reconnect();
                return null;
            }

            return instance;
        }
        Debug.LogError("GSM| GameSparks Not Initialized...");
        return instance;
    }

    #endregion

    void Awake()
    {
        if (instance == null)
        {
            // when the first GSM is activated, it should be null, so we create the reference
            Debug.Log("GSM| Singleton Initialized...");
            instance = this;
            DontDestroyOnLoad(this.gameObject); // gamesparks manager persists throughout game
            this.gameObject.AddComponent<GameSparksUnity>();
        }
        else
        {
            // if we load into a level that has the gamesparks manager present, it should be destroyed //
            // there can be only one! //
            Debug.Log("GSM| Removed Duplicate...");
            Destroy(this.gameObject);
        }
    }

    void Start ()
    {
        GS.GameSparksAvailable += (gsAvailable) =>
            {
                if (gsAvailable)
                {
                    Debug.Log(">>>>> GameSparks Connected <<<<<");
                } 
                else
                {
                    Debug.Log(">>>>> GameSparks Disconnected <<<<<");
                }
            };

        Setup_MSG_Delegates();
    }

    /// <summary>
    /// This callback is used where an error dialog needs to be presented when a request to GameSparks fails
    /// </summary>
    public delegate void OnError(string errorCode);

    #region GAMESPARKS AUTHENITCATION

	/// <summary>
	/// This class is used to contain all the player details for the current player
	/// such as the name, diamond balance and a list of virtual goods.
	/// </summary>
    
	public class PlayerDetails
	{
		public string displayName;
        public string userEmail;
        public string[] virtualGoods;
        public int score;
        public int level;
        //public int currentMask = 0;

        public PlayerDetails(string displayName, GSData responseData)
		{
			this.displayName = displayName;
            Debug.Log("PlayerDetails| displayName:" + displayName);

            if (responseData.GetString("email") != null) // balance might not be present if the player has just registered. so in this case, we  set the balance to zero
            {
                this.userEmail = responseData.GetString("email");
                Debug.Log("PlayerDetails| Email:" + userEmail);
            }
            /*
            if (responseData.GetGSData("TEST") != null) // balance might not be present if the player has just registered. so in this case, we  set the balance to zero
            {
                Debug.LogError("Bingo");
            }  
            */
            if (responseData.GetInt("score").HasValue) // balance might not be present if the player has just registered. so in this case, we  set the balance to zero
			{
				this.score = responseData.GetInt("score").Value;
                Debug.Log("PlayerDetails| score:" + this.score);
            }
            else
			{
				this.score = 0;
			}

            if (responseData.GetInt("level").HasValue) // balance might not be present if the player has just registered. so in this case, we  set the balance to zero
            {
                this.level = responseData.GetInt("level").Value;
                Debug.Log("PlayerDetails| Level:" + this.level);
            }
            else
            {
                this.level = 0;
            }
            /*
			// we need to check if the player has an VGs before constructing the list
			if(responseData.GetStringList("virtualGoods")  != null)
			{
				virtualGoods = responseData.GetStringList("virtualGoods").ToArray();
			}

            if (responseData.GetString("someString") != null) // balance might not be present if the player has just registered. so in this case, we  set the balance to zero
            {
                //string temp = responseData.GetString("currentMask").Replace("mask","");
                //Debug.Log("PlayerDetails| currentMask:" + temp);
                //this.currentMask = int.Parse(temp);
            }
            */
        }
    }

    /// <summary>
    /// Called when a player authenticates or registers with GameSparks
    /// </summary>
	public delegate void OnPlayerAuthenticated(PlayerDetails playerDetails);//string playerName, GSData userData = null);

    /// <summary>
    /// This method will call a registration request to GameSparks to automatically register a user when they log into the game.
    /// If the user is already registered, we call an authentication request to log the player in instead.
    /// both requests trigger a callback if they are succesful.
    /// </summary>
    /// <param name="displayName">Display name.</param>
    /// <param name="password">Password.</param>
    /// <param name="onPlayerAuth">Callback, returns player display name </param>
    public void AuthenticatePlayer(string displayName, string password, OnPlayerAuthenticated onPlayerAuth,OnError onAuthError,string emailAddress = null)
    {
        GSRequestData emailData = new GSRequestData();

        Debug.Log("GSM| Logging Player In With GameSparks....");
        if (emailAddress == null)
        {
            Debug.Log("GSM| No Email provided on Auth....");
        }
        else
        {
            emailData.Add("email", emailAddress);
        }

        new GameSparks.Api.Requests.RegistrationRequest()  // Attempt to Register a New Player
            .SetDisplayName(displayName)      // Set Display name
            .SetUserName(displayName)         // Set User Name
            .SetScriptData(emailData)//set user Email
            .SetPassword(password)         // Set Password
            .Send((regResponse) => {
				if(!regResponse.HasErrors && onPlayerAuth != null)
                {
                    Debug.LogWarning("GSM| Registration Succesful...");
					onPlayerAuth(new PlayerDetails(regResponse.DisplayName, regResponse.ScriptData));
                }
                else
                {
                    if(!(bool)regResponse.NewPlayer)  // If an Existing Player, use Auth instead                            
                    {
                        new GameSparks.Api.Requests.AuthenticationRequest()     // Attempt to Authenticate Existing Player  
                        .SetUserName(displayName)         // Set UserName
                        .SetScriptData(emailData)       //set user Email
                        .SetPassword(password)         // Set Password
                        .Send((authReponse) => {        // In Line Reference to Asynchronous Response
							if(!authReponse.HasErrors && onPlayerAuth != null)  // If no Errors in Repsonse
                            {
								onPlayerAuth(new PlayerDetails(authReponse.DisplayName, authReponse.ScriptData));
                            }
                            else  // If Errors in Response
                            {
                                Debug.Log("GSM| Error Authenticating Player /n " + authReponse.Errors.JSON);  // Log errors 
                                if (onAuthError != null)
                                {
                                    onAuthError(authReponse.Errors.JSON);
                                }
                            }
                        });
                    }
                }
            });
    }

   

    /// <summary>
    /// Connects GameSparks player to Facebook
    /// </summary>
    /// <param name="accessToken">Facebook AccessToken</param>
    /// <param name="onPlayerAuth">Callback, returns player display name </param>
    public void GameSparksFaceBookLogin(string accessToken, OnPlayerAuthenticated onPlayerAuthenticated,OnError onError)
    {
        Debug.Log("GS| GameSparks Connecting To Facebook...");
        new GameSparks.Api.Requests.FacebookConnectRequest()
            .SetSwitchIfPossible(true)
            .SetDoNotLinkToCurrentPlayer(false)
            .SetAccessToken(accessToken)
            .Send((fbConnectResp) => {
                if (!fbConnectResp.HasErrors)
                {
                    Debug.Log("GSM| Logged into GameSparks with FaceBook...");
                    if(onPlayerAuthenticated != null)
                    {
						onPlayerAuthenticated(new PlayerDetails(fbConnectResp.DisplayName, fbConnectResp.ScriptData));
                    }
                }
                else 
                {
                    if (onError != null)
                    {
                        onError("GSM| Error Logging in With FaceBook.../n" + fbConnectResp.Errors.JSON);
                        //onPlayerAuthenticated(new PlayerDetails(fbConnectResp.DisplayName, fbConnectResp.ScriptData));
                    }
                    //Debug.Log("GSM| Error Logging in With FaceBook.../n"+fbConnectResp.Errors.JSON);
                }
            });
    }

    #endregion

    #region GAMESPARKS SCORING / LEADERBOARDS
    /// <summary>
    /// Called when the GetBestScore request returns a valid score
    /// </summary>
    public delegate void OnGetBestScoreSuccess(int score);

    /// <summary>
    /// Requests the player's best score from GameSParks
    /// </summary>
    /// <param name="onSuccess">Callback, </param>
    /// <param name="onError">onBestScore, called when the request returns the best score</param>
    public void GetBestScore(string highScoreLeaderBoard, OnGetBestScoreSuccess onBestScore, OnError onError)
    {
        Debug.Log("GSM| Fetching Player Best Score...");

        new GameSparks.Api.Requests.GetLeaderboardEntriesRequest()
            .SetLeaderboards(new List<string> { highScoreLeaderBoard }) // create a new leaderboard list for the request. In this case the request takes a list, but only need to look for the highscore LB
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    // parse the player current rank on the LB and send it to the callback, but only if the value is present
                    if (response.BaseData.GetGSData(highScoreLeaderBoard).GetNumber("rank").HasValue)
                    {
                        int bestScore = (int)response.BaseData.GetGSData(highScoreLeaderBoard).GetNumber("score");
                        onBestScore(bestScore);
                    }
                    else if(onError != null)
                    {
                        onError("Player Has No Score Currently");
                    }
                }
                else
                {
                    if(onError != null)
                    {
                        Debug.LogWarning("GSM| Error Fetching Player Score... /n"+response.Errors.JSON);
                        onError("Error Fetching Player Score");
                    }
                }
            });
    }


    public delegate void OnScoreSuccess(GSData scriptData);
    /// <summary>
    /// Sends the player's score to the server
    /// </summary>
    /// <param name="score">Score.</param>
    public void SetBestScore(string eventShortCode,int score,OnScoreSuccess onSuccess,OnError onError)
    {
        Debug.Log("GSM| Setting Player Score...");
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey(eventShortCode)
            .SetEventAttribute("Score", score)
            .Send((response) =>
            {
                //Debug.Log(!response.HasErrors ? "GSM| Score posted!..." : "GSM| Error Posting Score... /n"+response.Errors.JSON );
                if (!response.HasErrors)
                {
                    if (onSuccess != null)
                    {
                        onSuccess(response.ScriptData);
                    }
                }
                else
                {
                    if (onError != null)
                    {
                        onError(response.Errors.JSON);
                    }
                }
            });
    }

    public void LevelUpUser()
    {
        Debug.Log("GSM|  Player Leveling Up...");
        new GameSparks.Api.Requests.LogEventRequest()
            .SetEventKey("LevelUpEvent")
            .Send((response) =>
            {
                if (!response.HasErrors)
                {
                    Debug.Log("GSM|  Player Leveled Up");
                    GameManager.Instance().currentPlayerDetails.level++;
                    GameManager.Instance().SetPlayerProfile(GameManager.Instance().currentPlayerDetails);
                }
                else
                {
                    Debug.Log(response.Errors.JSON);
                }
            });
    }

    /// <summary>
    /// Returns an array of leaderboard entries immedialty surrounding the player's rank
    /// </summary>
    /// <param name="lbShortcode">Lb shortcode.</param>
    /// <param name="onSuccess">On success. callback, takes leaderboard data array</param>
    /// <param name="onError">On error. callback, takes string - error code</param>
    public void GetAroundMeLeaderBoard(string lbShortcode, OnGetLeaderboardData onGetLB, OnError onError)
    {
        Debug.Log("GSM| Fetching Player Ranks Around This Player...");

        new GameSparks.Api.Requests.AroundMeLeaderboardRequest()
            .SetLeaderboardShortCode(lbShortcode)
            .SetEntryCount(11)
            .Send((response) =>
            {
                if (!response.HasErrors && onGetLB != null)
                {
                    // once we have the LB data from the server, we can create a new array out of it //
                    // we could just send the response back, but the UI only needs the rank, score and username so there is no need to have the whole response returned //
                    LeaderboardEntryData[] lbEntryDataArray = new LeaderboardEntryData[response.Data.Count()];
                    Debug.Log("GSM| " + lbEntryDataArray.Length.ToString()+ "LeaderboardEntryData");

                    for (int i = 0; i < lbEntryDataArray.Length; i++)
                    {
                        //Debug.LogWarning(response.Data.ElementAt(i).ToString());
                        lbEntryDataArray[i] = new LeaderboardEntryData(response.Data.ElementAt(i).Rank.ToString(), response.Data.ElementAt(i).UserName, response.Data.ElementAt(i).JSONData["Score"].ToString());
                        //Debug.LogWarning(response.Data.ElementAt(i).Rank.ToString() + response.Data.ElementAt(i).UserName);// + response.Data.ElementAt(i).JSONData["score"].ToString());
                    }
                    onGetLB(lbEntryDataArray);
                }
                else
                {
                    if(onError != null)
                    {
                        Debug.LogWarning("GSM| Error Fetching Player Score... /n"+response.Errors.JSON);
                        onError("Error acquiring lb data");
                    }
                }
            });
    }


    /// <summary>
    /// called when the leaderboard data is fecthed from the server. returns an array of leaderboard data
    /// </summary>
    public delegate void OnGetLeaderboardData(LeaderboardEntryData[] lbEntryData);

    /// <summary>
    /// This method will request the top 11 ranks in a given leaderboard and return an array of leaderboard data
    /// </summary>
    /// <param name="lbShortCode">Lb short code.</param>
    /// <param name="onGetLB">On get LB. callback, takes an array of leaderboard data</param>
    /// <param name="onError">On error.</param>
    public void GetTopLeaderBoard(string lbShortCode, OnGetLeaderboardData onGetLB, OnError onError)
    {
        Debug.Log("GSM| Fetching Top Player Ranks...");

        new GameSparks.Api.Requests.LeaderboardDataRequest()
            .SetLeaderboardShortCode(lbShortCode)
            .SetEntryCount(11)
            .Send((response) =>
            {
                if (!response.HasErrors && onGetLB != null)
                {
                    // once we have the LB data from the server, we can create a new array out of it //
                    // we could just send the response back, but the UI only needs the rank, score and username so there is no need to have the whole response returned //
                    LeaderboardEntryData[] lbEntryDataArray = new LeaderboardEntryData[response.Data.Count()];
                    //Debug.Log("GSM| lbEntryDataArray: " + lbEntryDataArray.Length);

                    for (int i = 0; i < lbEntryDataArray.Length; i++)
                    {
                        lbEntryDataArray[i] = new LeaderboardEntryData(response.Data.ElementAt(i).Rank.ToString(), response.Data.ElementAt(i).UserName, response.Data.ElementAt(i).JSONData["Score"].ToString());
                    }
                    onGetLB(lbEntryDataArray);
                }
                else
                {
                    if(onError != null)
                    {
                        Debug.LogWarning("GSM| Error Fetching Player Score... /n"+response.Errors.JSON);
                        onError("Error acquiring lb data");
                    }
                }
            });
    }

    /// <summary>
    /// Leaderboard entry data.
    /// Used in order for the LB requests to be able to return the same type of LB data instead of one for each response
    /// </summary>
    public class LeaderboardEntryData
    {
        public string rank;
        public string userName;
        public string score;

        public LeaderboardEntryData(string rank, string userName, string score)
        {
            this.rank = rank;
            this.userName = userName;
            this.score = score;
        }

    }
    #endregion

    #region GAMESPARKS CURRENCY / VIRTUALGOODS
    /// <summary>
    /// Called when Currency1/Diamonds are credited to the player.
    /// </summary>
    /// <param name="response"></param>
    public delegate void OnCreditDiamondsSuccess(int newBalance);

    /// <summary>
    /// Credits the Player with Currency1/Diamonds
    /// </summary>
    /// <param name="amount">The amount of currency1/Diamonds to be credited</param>
    /// <param name="onCreditSuccess">On Credit Diamonds Callback</param>
    /// <param name="onError">On Error</param>
    /*
    public void AdjustDiamonds(int amount, OnCreditDiamondsSuccess onCreditSuccess, OnError onError)
    {
        new GameSparks.Api.Requests.LogEventRequest()
           .SetEventKey("adjustDiamonds")
           .SetEventAttribute("amount", amount)
           .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   if (onCreditSuccess != null)
                   {
                       onCreditSuccess((int)response.ScriptData.GetInt("Balance"));
                   }
               }
               else
               {
                   if (onError != null)
                   {
                       onError("Error Crediting Player Diamonds: / n" + response.Errors.JSON);
                   }
               }
           });
    }
    */
    /// <summary>
    /// Called on successfull virtual Goods purchase
    /// </summary>
    /// <param name="boughtItems">The Virtual goods purchased</param>
    /// <param name="currencyConsumed">Currency1/Diamonds spent</param>
    public delegate void OnPurchaseVirtualGoodsSuccess(GSEnumerable<BuyVirtualGoodResponse._Boughtitem> boughtItems, int currencyConsumed);

    /// <summary>
    /// Purchase a Virtual Good
    /// </summary>
    /// <param name="virtualGoodID">The Virtual Good shortCode</param>
    /// <param name="onPurchaseSuccess">On Purchase Callback</param>
    /// <param name="onError">On Error</param>
    public void PurchaseVirtualGoods(string virtualGoodID, OnPurchaseVirtualGoodsSuccess onPurchaseSuccess, OnError onError)
    {
        new GameSparks.Api.Requests.BuyVirtualGoodsRequest()
            .SetCurrencyType(1)
            .SetQuantity(1)
            .SetShortCode(virtualGoodID)
            .Send((response) => 
            {
                if (!response.HasErrors)
                {
                    if (onPurchaseSuccess != null)
                    {
                        GSEnumerable<GameSparks.Api.Responses.BuyVirtualGoodResponse._Boughtitem> boughtItems = response.BoughtItems;
                        int currencyConsumed = (int)response.CurrencyConsumed;

                        onPurchaseSuccess(boughtItems,currencyConsumed);
                    }
                }
                else
                {
                    if (onError != null)
                    {
                        onError("Error Crediting Player Diamonds:/ n" + response.Errors.JSON);
                    }
                }
               
        });
    }

	/// <summary>
	/// On list virtual goods.
	/// </summary>
	public delegate void OnListVirtualGoods(ListVirtualGoodsResponse._VirtualGood[]  virtualGoods);

    /// <summary>
    /// List virtual Goods in Config.
    /// </summary>
    /// <param name="onError">OnError</param>
	public void ListVirtualGoods(OnListVirtualGoods onListVGs, OnError onError)
    {
        new GameSparks.Api.Requests.ListVirtualGoodsRequest()
            .Send((response) =>
            {
				if (!response.HasErrors && onListVGs != null)
                {
					onListVGs(response.VirtualGoods.ToArray());
                }
                else
                {
                    if (onError != null)
                    {
						Debug.LogWarning("GSM| ListedVirtualGoods: /n" + response.JSONString);
						onError("Error Listing VirtualGoods");
                    }
                }
            });
    }
    #endregion

    void Setup_MSG_Delegates()
    {
        GameSparks.Api.Messages.AchievementEarnedMessage.Listener += AchievementEarned_MessageHandler;
    }

    public void AchievementEarned_MessageHandler(GameSparks.Api.Messages.AchievementEarnedMessage message)
    {
        GameSparksUI.Instance().OnAchievementEarned(message);
    }

    public void MatchMakingAttempt(string matchShortCode, string matchGroup = null,string action = null,GSRequestData matchData = null)
    {
        //GSRequestData matchData = new GSRequestData();
        //matchData.Add("WibbleKey", "WibbleValue");

        new GameSparks.Api.Requests.MatchmakingRequest()
            .SetMatchShortCode("Wibble")
            .SetMatchGroup("Wibble")
            .SetAction("Wibble")
            .SetMatchData(matchData)
             .Send((response) =>
             {
                 if (!response.HasErrors)
                 {

                 }
                 else
                 {

                 }

             });
    }

    public delegate void OnGetServerTimeSuccess(GSData timeData);

    public void Get_ServerTime(OnGetServerTimeSuccess onSuccess,OnError onError)
    {
        new LogEventRequest()
           .SetEventKey("ServerTime")
           .Send((response) =>
           {
               if (!response.HasErrors)
               {
                   if (onSuccess != null)
                   {
                       onSuccess(response.ScriptData);
                   }
               }
               else
               {
                   if (onError != null)
                   {
                       onError("Error getting ServerTime: " + response.Errors.JSON);
                   }
               }
           });
    }

    void EndSession()
    {
        new GameSparks.Api.Requests.EndSessionRequest()
            .SetDurable(true)
             .Send((response) =>
             {
                 if (!response.HasErrors)
                 {

                 }
                 else
                 {

                 }

             });
    }
}
