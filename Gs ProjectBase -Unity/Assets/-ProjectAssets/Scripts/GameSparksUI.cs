﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;
using System;
using UnityEngine.SceneManagement;
using GameSparks.Core;
// AppAdvisory.GeometryJump;


/// <summary>
/// Game sparks UI.
/// 
/// This class handles all the buttons and UI fields for the GameSparks and FB login
/// 
/// Created by Sean Durkan, Feb 2017 for GameSparks
/// </summary>
public class GameSparksUI : MonoBehaviour 
{
    #region Singleton
    private static GameSparksUI instance = null;

    public static GameSparksUI Instance()
    {
        if (instance != null)
        {
            return instance;
        }
        Debug.LogError("GSUI| UI Not Initialized...");
        return instance;
    }
    #endregion

    public enum UiState
    {
        MainMenu,
        InGame,
        InGameMenu,
        Leaderboard,
        LeaderboardToGame,
        VideoContent
    }

    [Header("UiState:")]
    public UiState uiState;

    [Header("Menu UI Elements:")]
    public GameObject mainMenuUi_Holder;
    public Button gamesparksLogin_bttn;
    public Button facebookLogin_bttn;
    public Button quit_bttn;

    public InputField displayNameField;
    public InputField passwordField;
    public InputField emailField;

    public Toggle rememberMeToggle;

    [Header("Game Holder:")]
    public GameObject inGameUi_Holder;
    [Header("Menu Holder:")]
    public GameObject gameMenuUi_Holder;
    [Header("Achievement Elements:")]
    public GameObject achievement_LayoutGroup;
    public GameObject achievementToast_Prefab;
    [Header("LeaderBoard Elements:")]
    public Transform lbGroup;
    public Scrollbar lb_scrollBar;
    public Dropdown lb_dropdown;
    [Header("VideoContent")]
    public GameObject videoContentUI_Group;
    public GameObject videoContent;

    void Awake()
    {
        uiState = UiState.MainMenu;
        /// Checks if the player is using the toggle and if not, we clear the player prefs so there is no automatic login
        if (!rememberMeToggle.isOn)
        {
            PlayerPrefs.DeleteAll();
            Debug.LogWarning("GSUI| Cleared Login Details...");
        }
    
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject); // gamesparks manager persists throughout game
        }

        if (uiState == UiState.MainMenu)
        {
            inGameUi_Holder.SetActive(false);
            gameMenuUi_Holder.SetActive(false);

            mainMenuUi_Holder.SetActive(true);
        }
    }
        
	void Start () 
    {
        //Check for stored Values
        RetrieveUserDetails();
        // add a listener to the gamesparks login button to call the RPC authentication call when the player logs in
        gamesparksLogin_bttn.onClick.AddListener(() =>
        {
            AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);

            string email = emailField.text == null ? null : emailField.text;

            gamesparksLogin_bttn.interactable = false;
            gamesparksLogin_bttn.transform.GetChild(0).GetComponent<Text>().text = "Logging In...";
			GameSparksManager.Instance().AuthenticatePlayer(displayNameField.text, passwordField.text, OnPlayerLoggedIn,OnAuthError, email);
        });
        // when the facebook login button is called, facebook need to be initialized first, so we call that method here
        facebookLogin_bttn.onClick.AddListener(() =>
        {
            AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);
            //facebookLogin_bttn.interactable = false;
            InitializeFacebook();
        });
	}

    void RetrieveUserDetails()
    {
        // if we have a saved username, then load it into the username field
        if (PlayerPrefs.GetString("userName") != null && PlayerPrefs.GetString("userName") != string.Empty)
        {
            Debug.Log("GSUI| User details found for " + PlayerPrefs.GetString("userName"));
            displayNameField.text = PlayerPrefs.GetString("userName");

            if (PlayerPrefs.GetString("email") != null && PlayerPrefs.GetString("email") != string.Empty)
            {
                Debug.Log("GSUI| User Email details found for " + PlayerPrefs.GetString("userName"));
                emailField.text = PlayerPrefs.GetString("email");
            }
        }
        else
        {
            Debug.Log("GSUI| No User details stored...");
        }
    }

    /// <summary>
    /// Called when GameSparksManager returns a valid authentication or registration.
    /// </summary>
    /// <param name="playerId">Player ID</param>
    private void OnPlayerLoggedIn(GameSparksManager.PlayerDetails playerDetails)
    {

		Debug.Log("GSUI| Player ["+playerDetails.displayName+"] Logged In....");
        if(PlayerPrefs.GetString("userName") != null)
        {
            Debug.Log("GSUI| Saving Player Login...");
            PlayerPrefs.SetString("userName", displayNameField.text);
        }

        if (PlayerPrefs.GetString("email") != null && emailField.text != null && emailField.text != string.Empty)
        {
            Debug.Log("GSUI| Saving Player Email...");
            PlayerPrefs.SetString("email", emailField.text);
        }

        GameManager.Instance().StorePlayerProfile(playerDetails);

        #region
        //Debug.Log("GSUI| Player [" + playerDetails.playerName + "] DiamondBalance:" + playerDetails.diamondBalance);
        //Player Score
        //playerDiamondBalance = playerDetails.diamondBalance;
        //currentPlayerDiamondBalance = playerDetails.diamondBalance;
        /*
		// player virtual goods/masks //
		string virtulaGoods = string.Empty;

		for (int i = 0; i < playerDetails.virtualGoods.Length; i++) 
		{
            virtulaGoods += playerDetails.virtualGoods [i]+",";
		}
        */
        //Debug.Log ("GSUI| Player Masks:"+playerMasks);
        //currentMask = playerDetails.currentMask;
        #endregion
        // load scene //
        //SceneManager.LoadScene("GeometryJump");
        AudioManager.Instance().PlaySfx(AudioManager.Instance().positive_Sfx);

        SwitchUi(UiState.InGameMenu);
        Retrieve_ServerTime();
    }

    private void OnAuthError(string error)
    {
        Debug.LogWarning("GSUI| Error Authenticating Player:" + error);  // Log errors 
        AudioManager.Instance().PlaySfx(AudioManager.Instance().negitive_Sfx);
        //Could do error handling here....
        gamesparksLogin_bttn.interactable = true;
        gamesparksLogin_bttn.transform.GetChild(0).GetComponent<Text>().text = "LogIn";
    }

    /// <summary>
    /// Called when Ui button for FaceBookAuthentication
    /// </summary>
    /// <param name="playerId">Player ID</param>
    public void InitializeFacebook()
    {
        if (!FB.IsInitialized)
        {
            Debug.Log("GSM| Initializing FaceBook...");
            FB.Init(FaceBookLogin);
        }
        else
        {
            FaceBookLogin();
        }
    }

    private void FaceBookLogin()
    {
        Debug.Log("GSM| Logging into FaceBook...");
       
        // we need to setup the permission we need to request FB for this user's details //
        // after that we will immedialty connect this player to GameSparks, so we call this method in the FB-login callback
        FB.LogInWithReadPermissions(
            new List<string>() { "public_profile", "email", "user_friends" },
            GameSparksFaceBookConnect
        );
    }

    private void GameSparksFaceBookConnect(ILoginResult result)
    {
        if (FB.IsLoggedIn)
        {
            Debug.LogWarning("GSUI| Logging into GameSparks with FB details...");
            GameSparksManager.Instance().GameSparksFaceBookLogin(AccessToken.CurrentAccessToken.TokenString, OnPlayerLoggedIn, OnAuthError);
        }
        else
        {
            {
                Debug.Log("GSUI| Error Logging Into FaceBook /n" + result.Error);
            }
        }
    }

    #region LeaderBoards

    public enum LeaderBoardType
    {
        AroundMeLeaderboard,
        TopScoreLeaderboard
    }

    /*  //Overload for Get and Draw
     public void GetAndDrawLeaderBoard(string shortCode,Transform lbGroup, LeaderBoardType lbType)
     {
         // first we need to get the layout group object from the scene //
         // this gameobject has another 2 objects inside, so we only want the first one, as this is where the LB entries are //
         Transform lbLayout = lbGroup.GetComponentInChildren<VerticalLayoutGroup>().transform;//.transform;//.GetChild(0);
         lbGroup.gameObject.SetActive(true); // turn on the lbGroup so it is not hidden

         if(lbType == LeaderBoardType.TopScoreLeaderboard)
         {
             GameSparksManager.Instance().GetAroundMeLeaderBoard(shortCode,
                 (entryList) =>
                 {
                     DrawLeaderboardEntries(lbLayout, entryList);
                 },
                 (errorCode) =>
                 {
                     Debug.LogWarning(errorCode);
                 });
         }
         else
         {
             GameSparksManager.Instance().GetTopLeaderBoard("highScoreLeaderBoard",
                 (entryList) =>
                 {
                     DrawLeaderboardEntries(lbLayout, entryList);
                 },
                 (error) =>
                 {
                     Debug.LogWarning(error);
                 });
         }
     }
    */

    public void OnClicked_LeaderboardDropDown()
    {
        int value = lb_dropdown.value;
        
        GameObject dropList = lb_dropdown.transform.FindChild("Dropdown List").gameObject;
        Destroy(dropList);

        if (value == 0)
        {
            Debug.Log("GSUI | OnClicked_LeaderboardDropDownAroundMe!");
            OnClicked_PullaroundMeLeaderBoard();
        }
        else if(value == 1)
        {
            Debug.Log("GSUI|OnClicked_LeaderboardDropDown-TopTen!");
            OnClicked_PullTopLeaderBoard();
        }
    }

    public void OnClicked_PullaroundMeLeaderBoard()
    {
        Debug.LogWarning("GSUI| OnClicked_PullaroundMeLeaderBoard");
        AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);

        // call the gamesparks ui manager to get the lb-data and draw it onto the current lb-layout group
        GetAndDrawLeaderBoard(lbGroup, GameSparksUI.LeaderBoardType.AroundMeLeaderboard);
        //Ui change
        SwitchUi(UiState.Leaderboard);
    }

    public void OnClicked_PullTopLeaderBoard()
    {
        AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);

        // call the gamesparks ui manager to get the lb-data and draw it onto the current lb-layout group
        GetAndDrawLeaderBoard(lbGroup, GameSparksUI.LeaderBoardType.TopScoreLeaderboard);
        //Ui change
        SwitchUi(UiState.Leaderboard);
    }

    public void OnClicked_BackFromLeaderBoard()
    {
        //clean Leaderboard Ui and reset for next use
        Transform vGroup = lbGroup.GetComponentInChildren<VerticalLayoutGroup>().transform;

        foreach (Transform child in vGroup)
        {
            EnablePlayerEntryIndicator(child, false);
            child.gameObject.SetActive(false);
        }

        AudioManager.Instance().PlaySfx(AudioManager.Instance().buttonClick_Sfx);

        if (GameManager.Instance().answered != 0)
        {
            SwitchUi(UiState.LeaderboardToGame);
        }
        else
        {
            SwitchUi(UiState.InGameMenu);
        }
    }

    /// <summary>
    /// Fetches the chosen LB type from GameSparks and draws the Lb entries upon the given layout group
    /// </summary>
    /// <param name="lbGroup">Lb group.</param>
    /// <param name="lbType">Lb type. and enum used to denote which type of LB we want the UI to draw</param>
    public void GetAndDrawLeaderBoard(Transform lbGroup, LeaderBoardType lbType)
    {
        // first we need to get the layout group object from the scene //
        // this gameobject has another 2 objects inside, so we only want the first one, as this is where the LB entries are //
        Transform lbLayout = lbGroup.GetComponentInChildren<VerticalLayoutGroup>().transform;// lbGroup.transform;//.GetChild(0);

        if (lbType == LeaderBoardType.AroundMeLeaderboard)
        {
            // get the right LB type for GameSparks to request
            GameSparksManager.Instance().GetAroundMeLeaderBoard("highScoreLeaderBoard",
                (entryList) =>
                {
                    DrawLeaderboardEntries(lbLayout, entryList);
                },
                (errorCode) =>
                {
                    Debug.LogWarning(errorCode);
                });
        }
        else if(lbType == LeaderBoardType.TopScoreLeaderboard)
        {
            GameSparksManager.Instance().GetTopLeaderBoard("highScoreLeaderBoard",
                (entryList) =>
                {
                    DrawLeaderboardEntries(lbLayout, entryList);
                },
                (error) =>
                {
                    Debug.LogWarning(error);
                });
        }

        Debug.LogWarning("GSUI| GetAndDrawLeaderBoard " + lbType.ToString());
    }

    /// <summary>
    /// Draws the leaderboard entries.
    /// The LB group is attached to the canvas manager in the main scene.
    /// this method gets the correct  component on each entry-field and draw it.
    /// </summary>
    /// <param name="lbEntryField">Lb entry field.</param>
    /// <param name="lbEntry">Lb entry.</param>
    private void DrawLeaderboardEntries(Transform lbLayout, GameSparksManager.LeaderboardEntryData[] entryList)
    {
        string userName = PlayerPrefs.GetString("userName");// stored username

        for (int i = 0; i < lbLayout.transform.childCount; i++)
        {
            Transform lbEntryField = lbLayout.transform.GetChild(i).transform;
            if(entryList.Length > i &&  entryList[i] != null)
            {
                lbEntryField.gameObject.SetActive(true);

                lbEntryField.transform.GetChild(0).GetComponent<Text>().text = entryList[i].rank;
                lbEntryField.transform.GetChild(1).GetComponent<Text>().text = entryList[i].userName;
                lbEntryField.transform.GetChild(2).GetComponent<Text>().text = entryList[i].score;

                if (entryList[i].userName == userName)//if there is an entry from the current user, enable the indicator
                {
                    Debug.Log("GSUI| Player entery found: " + entryList[i].userName);
                    EnablePlayerEntryIndicator(lbEntryField,true);
                }
            }
            else
            {
                if(lbEntryField.gameObject.name.Contains("LeaderBoardEntry"))//only disable unused entries.
                lbEntryField.gameObject.SetActive(false); // if there isnt a score present then disable that field
            }
        }

        lb_scrollBar.value = 1;//Set to top of the scoll area
    }

    void EnablePlayerEntryIndicator(Transform entry,bool enabled)
    {
        entry.transform.GetChild(3).gameObject.SetActive(enabled);
    }
    #endregion

    #region VitualGoods

    /// <summary>
    /// Wrapper method to call BuyVirtualgoodsRequest
    /// </summary>
    /// <param name="goodShortCode">The shortcode of the item to be purchased</param>
    public void Purchase(string goodShortCode)
    {
        GameSparksManager.Instance().PurchaseVirtualGoods(goodShortCode, OnPurchaseSuccess, OnPurchaseError);
    }

    /// <summary>
    /// Called on successful purchase
    /// </summary>
    /// <param name="boughtItems"></param>
    /// <param name="currencyConsumed"></param>
    public void OnPurchaseSuccess(GameSparks.Core.GSEnumerable<GameSparks.Api.Responses.BuyVirtualGoodResponse._Boughtitem> boughtItems, int currencyConsumed)
    {
        foreach (var item in boughtItems)
        {
            Debug.Log("GSUI| Purchased [" + item +"] for " + currencyConsumed);
        }
    }

    /// <summary>
    /// Called on Purchase faliure
    /// </summary>
    /// <param name="error">OnError</param>
    public void OnPurchaseError(string error)
    {
        Debug.Log("GSUI| Purchase Error /n" + error);
    }
    #endregion

    /// <summary>
    /// Ui state control
    /// </summary>
    /// <param name="toState"></param>
    public void SwitchUi(UiState toState)
    {
        switch (toState)
        {
            case UiState.MainMenu:
                GameManager.Instance().logoAnimator.SetBool("Active", false);
                videoContent.SetActive(false);
                videoContentUI_Group.SetActive(false);
                mainMenuUi_Holder.SetActive(true);               
                break;
            case UiState.InGame:
                //mainMenuUi_Holder.SetActive(false);
                //gameMenuUi_Holder.SetActive(true);
                //GameManager.Instance().InitGame();
                //inGameUi_Holder.SetActive(true);
                break;
            case UiState.InGameMenu:
                GameManager.Instance().logoAnimator.SetBool("Active", true);
                videoContent.SetActive(false);
                videoContentUI_Group.SetActive(false);
                mainMenuUi_Holder.SetActive(false);
                inGameUi_Holder.SetActive(false);
                gameMenuUi_Holder.SetActive(true);
                lbGroup.gameObject.SetActive(false); // turn off the lbGroup so it is hidden
                break;
            case UiState.Leaderboard:
                //mainMenuUi_Holder.SetActive(false);
                inGameUi_Holder.SetActive(false);
                gameMenuUi_Holder.SetActive(false);
                lbGroup.gameObject.SetActive(true);
                break;
            case UiState.LeaderboardToGame:
                lbGroup.gameObject.SetActive(false);
                gameMenuUi_Holder.SetActive(true);
                inGameUi_Holder.SetActive(true);
                break;
            case UiState.VideoContent:
                lbGroup.gameObject.SetActive(false);
                gameMenuUi_Holder.SetActive(false);
                inGameUi_Holder.SetActive(false);
                videoContent.SetActive(true);
                videoContentUI_Group.SetActive(true);
                break;
            default:
                Debug.LogError("VertDeFurk!!!?");
                break;
        }
    }

    public void OnAchievementEarned(GameSparks.Api.Messages.AchievementEarnedMessage message)
    {
        string name = "";
        string description = "";

        switch (message.AchievementName)
        {
            case "New Player":
                name = "New Player Achievement!";
                description = "Welcome!";
                break;
            case "Quiz Complete":
                name = "Quiz Complete!";
                description = "A great start!";
                break;
            case "Level Up":
                name = "Level Up!";
                description = "You have reached Level:"+(GameManager.Instance().currentPlayerDetails.level).ToString() + "!";
                break;
            default:
                Debug.LogError("GSUI| OnAchievementEarned");
                break;
        }

        GameObject go = Instantiate(achievementToast_Prefab, achievement_LayoutGroup.transform.position, Quaternion.identity);
        go.GetComponent<Utility_Achievement>().SetAchievementUI(name, description);
        go.transform.SetParent(achievement_LayoutGroup.transform,false);
    }

    #region Time
    public void Retrieve_ServerTime()
    {
        GameSparksManager.Instance().Get_ServerTime(OnServerTimeSuccess, OnServerTimeError);
    }

    private void OnServerTimeSuccess(GSData data)
    {
        double serverTime = (double)data.GetDouble("ServerTime");
        DateTime correctedTime = Get_UtcDate(serverTime);
        Debug.Log("GSUI |LocalTime: " +DateTime.Now+ " ServerTime: " + correctedTime);
    }

    private void OnServerTimeError(string error)
    {
        Debug.LogWarning("GSUI| Error getting ServerTime:" + error);  // Log errors 
        AudioManager.Instance().PlaySfx(AudioManager.Instance().negitive_Sfx);
    }

    public DateTime Get_UtcDate(double serverTime)
    {
        //Double utcTime = 111111;//DateTime.UtcNow;//Testing.... this should be the utc Time returned from GS
        //DateTime.UtcNow.ToString("HH:mm dd MMMM, yyyy") // returns "12:48 15 April, 2013" 
        DateTimeKind kind = DateTimeKind.Utc;
        DateTime endTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, kind);//Date time is ticks,so i needtoconstruct a new Utc date tiem starting from beginning of utc
        return endTime.AddMilliseconds(serverTime).ToLocalTime();
    }

    #endregion

    public void DisconnectFromGameSparks()
    {
        GameSparks.Core.GS.Disconnect();
    }

    public void QuitToDesktop()
    {
        //DisconnectFromGameSparks();
        StartCoroutine(QuitApplication());
        Debug.Log("GSUI| QuitToDesktop");
    }

    IEnumerator QuitApplication()
    {
        yield return new WaitForSeconds(0.2f);
        AudioManager.Instance().PlaySfx(AudioManager.Instance().positive_Sfx);
        yield return new WaitForSeconds(0.3f);
        Debug.Log("GSUI| Closing Application");
        Application.Quit();
    }
}
