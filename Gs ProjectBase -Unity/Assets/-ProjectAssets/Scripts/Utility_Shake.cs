﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Utility_Shake : MonoBehaviour {

    public float time = 1f;
    public float intensity = 1f;

    public bool isActive = false;
    Vector3 originalPosition;

    void Start()
    {
        originalPosition = transform.position;
    }

	void Update ()
    {
        if (isActive)
        {
            if (time > 0)
            {
                time -= Time.deltaTime;
                Vector3 random = Random.insideUnitCircle * intensity;
                transform.position = originalPosition + random;
            }
            else
            {
                transform.position = originalPosition;
                isActive = true;
            }
        }
	}

    public void Shake(float duration, float strenght)
    {
        time = duration;
        intensity = strenght;
        isActive = true;
    }
}
