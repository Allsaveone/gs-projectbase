﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Utility_Achievement : MonoBehaviour {

    public Animator anim;
    public Text title_Text;
    public Text flavour_Text;
    public Image achievement_Icon;
    public float achievementDelay = 2f;

    public void SetAchievementUI(string title,string flavour,Sprite icon = null)
    {
        title_Text.text = title;
        flavour_Text.text = flavour;
        StartCoroutine(DestroyAfterAnimation());
    }

    IEnumerator DestroyAfterAnimation()
    {
        AudioManager.Instance().PlaySfx(AudioManager.Instance().positive_Sfx);

        yield return new WaitForSeconds(achievementDelay);
        anim.SetBool("Active", false);
        yield return new WaitForSeconds(achievementDelay);
        Destroy(this.gameObject);
    }
}
