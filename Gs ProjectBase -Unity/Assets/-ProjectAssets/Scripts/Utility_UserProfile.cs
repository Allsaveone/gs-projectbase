﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Utility_UserProfile : MonoBehaviour {

    public Image profilePic_Image;
    public Text displayName_Text;
    public Text level_Text;
    public Text score_Text;

    private int _level;
    private int _score;

    public void SetUserProfileUI(string displayName, int level, int score, Sprite profilePic = null)
    {
        _level = level;
        _score = score;

        displayName_Text.text = displayName;
        level_Text.text = "Lvl:" + level.ToString();
        score_Text.text = "Points:" + score.ToString();

        if (profilePic)
        {
            profilePic_Image.sprite = profilePic;
        }
    }

    public void AddScore(int award)
    {
        _score += award;
        score_Text.text = "Points:" + _score.ToString();
    }
}


