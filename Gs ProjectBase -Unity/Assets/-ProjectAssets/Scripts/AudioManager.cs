﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour {

    #region Singleton
    private static AudioManager instance = null;

    public static AudioManager Instance()
    {
        if (instance != null)
        {
            return instance;
        }
        Debug.Log("AM| Not Initialized...");
        return instance;
    }
    #endregion

    [Header("Audio Clips")]
    public AudioClip buttonClick_Sfx;
    public AudioClip buttonOver_Sfx;

    public AudioClip positive_Sfx;
    public AudioClip negitive_Sfx;

    public AudioClip completed_Sfx;

    [Header("Audio Clips")]
    public AudioSource sfx_Source;

    void Awake()
    {
        if (instance == null)
        {
            Debug.Log("AM| Singleton Initialized...");
            instance = this;
        }
        else
        {
            Debug.Log("AM| Removed Duplicate...");
            Destroy(this.gameObject);
        }
    }

    void Start () {
		
	}
	
	public void PlaySfx (AudioClip sfxClip)
    {
        sfx_Source.Stop();
        //sfx_Source.pitch = Random.Range(0.9f, 1.05f);
        sfx_Source.clip = sfxClip;
        sfx_Source.Play();
    }

    public void MouseOverUI()
    {
        PlaySfx(buttonOver_Sfx);
    }
}
